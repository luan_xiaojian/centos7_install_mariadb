#!/bin/bash
#centos7安装mariadb
#author:LXJ QQ：174686670
#^_^

#先删除之前centos自带的mariadb
rpm -qa |grep mariadb |xargs -i rpm -e {} --nodeps




#默认情况是数据库安装包已经上传到指定文件夹
#安装包存放文件夹
filepath=/tools/ 
#要按装的数据库安装包
#file=mariadb-10.3.14-linux-x86_64.tar.gz
file=mariadb-5.5.56-linux-x86_64.tar.gz

#新建数据库目录：
mkdir -p /data/mysql_data

#准备用户：-r创建系统用户，-d指定家目录，-m强制创建家目录，-s指定shell不允许这个用户登录（这样有坑^-^）
#useradd -r -d /home/mysql -m -s /sbin/nologin mysql
useradd -s /sbin/nologin mysql


#解压数据库文件
tar -zxf $filepath$file -C /usr/local
arr=$(echo $file | awk -F '.tar' {'print $1'})
mv /usr/local/${arr[0]} /usr/local/mysql


#准备my.cnf文件
yes |cp /usr/local/mysql/support-files/my-huge.cnf /etc/my.cnf

#修改my.cnf文件中的数据库目录
sed -i '0,/datadir/{/datadir/d}' /etc/my.cnf && sed -i '/^\[mysqld\]/a\datadir = /data/mysql_data\nlog-warning=2' /etc/my.cnf





#初始化数据库
cd /usr/local/mysql/ && ./scripts/mysql_install_db  --user=mysql --datadir=/data/mysql_data


#准备数据库文件
mkdir /var/log/mariadb/ && chown mysql.mysql /var/log/mariadb/


#准备启动文件
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld


#添加环境变量
echo "export PATH=/usr/local/mysql/bin:\$PATH" >> /etc/profile 
source /etc/profile

#启动数据库
#systemctl start mysqld
#/etc/init.d/mysqld start
#添加开机启动
chkconfig --add mysqld




